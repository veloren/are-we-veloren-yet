+++
title = "Teams"
description = "The different teams and topics that Veloren has."
page_template = "categories/page.html"

[extra]
icon = "cubes"
single = "article"
plural = "articles"
+++
